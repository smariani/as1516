/**
 *
 */
package sa.lab.tucson.swarms.launchers;

import java.util.logging.Level;
import java.util.logging.Logger;
import sa.lab.tucson.swarms.ants.Swarm;
import sa.lab.tucson.swarms.env.Environment;
import sa.lab.tucson.swarms.gui.GUI;
import sa.lab.tucson.swarms.utils.Topology;

/**
 * @author ste
 *
 */
public class LaunchSwarmsScenario {

    /**
     * @param args
     */
    public static void main(final String[] args) {

        Logger.getAnonymousLogger().log(Level.INFO, "Booting topology...");
        Topology.bootTopology();
        Logger.getAnonymousLogger().log(Level.INFO, "...topology boot");

        Environment.config();

        GUI.init();

        Swarm.release();

    }

}
