/*
 * Copyright 1999-2015 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of TuCSoN4Jason <http://tucson4jason.apice.unibo.it>.
 *
 *    TuCSoN4Jason is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    TuCSoN4Jason is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with TuCSoN4Jason.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
 
/* Agent 'bookBuyerAgent' in project t4jn_bookTrading.mas2j */

/* Initial beliefs and rules */
best_seller(null).
best_price(null).

/* Initial goals */
!boot.

/* Plans */
+!boot
    <- .print("Started.");
       .random(Index);
       .nth(
            math.round(Index * 6),
            [
                entry("harry_potter_and_the_sorcerer_stone", "8.79", "9.79", "10.79", "11.79", "12.79"),
                entry("harry_potter_and_the_chamber_of_secrets", "8.79", "9.79", "10.79", "11.79", "12.79"),
                entry("harry_potter_and_the_prisoner_of_azkaban", "8.79", "9.79", "10.79", "11.79", "12.79"),
                entry("harry_potter_and_the_goblet_of_fire", "10.39", "11.39", "12.39", "13.39", "14.39"),
                entry("harry_potter_and_the_order_of_the_phoenix", "10.39", "11.39", "12.39", "13.39", "14.39"),
                entry("harry_potter_and_the_half_blood_prince", "10.39", "11.39", "12.39", "13.39", "14.39"),
                entry("harry_potter_and_the_deathly_hallows", "10.19", "11.19", "12.19", "13.19", "14.19")
            ],
            Book);
       t4jn.api.getArg(Book, 0, Title); // seamless integration with Jason tuples
       .print("Setting book ", Title, " as desired book...");
       +target_book_title(Title);
       !wait_for_sellers.

+!wait_for_sellers
    <- .my_name(Me);
       .print("Searching 'book-trading' services in the 'default' tuple centre...");
       t4jn.api.rdAll("default", "127.0.0.1", "20504",
            advertise(provider(S),service(book_trading)), RdAll0); // async invocation
       t4jn.api.getResult(RdAll0, Res0); // sync result
       if ( .list(Res0) & (not .empty(Res0)) ) { // rd_all does not suspend, but returns empty lists
            for ( .member(S,Res0) ) { // store every seller as a mental note
                t4jn.api.getArg(S, 0, ProviderS);      // provider(S)
                t4jn.api.getArg(ProviderS, 0, Seller); // S
                .print("Agent ", Seller, " found.");
                +seller(Seller); // store seller as a mental note
            }
            !send_cfp;
       } else {
            .print("No suitable services found, retrying in 10 seconds...");
            .wait(10000); // wait 10 seconds
            !wait_for_sellers;
       }.

+!send_cfp
    <- .my_name(Me);
       ?target_book_title(TargetBookTitle);
       .print("Sending CFP for book ", TargetBookTitle, " to available seller agents...");
       .findall(X, seller(X), Sellers); // find all sellers stored as mental notes
       for ( .member(Seller, Sellers) ) {
            t4jn.api.out("default", "127.0.0.1", "20504",
                cfp(to(Seller), from(Me), book(TargetBookTitle)), Out0); // async invocation
       }
       !collect_proposals(TargetBookTitle).

+!collect_proposals(TargetBookTitle)
    <- .print("Waiting for proposals...");
       t4jn.api.in("default", "127.0.0.1", "20504",
            proposal(to(Me), book(TargetBookTitle), from(S), price(P)), In0);
       t4jn.api.getResult(In0, Res0);

       t4jn.api.getArg(Res0, 1, BookT);      // book(T)
       t4jn.api.getArg(BookT, 0, BookTitle); // T

       t4jn.api.getArg(Res0, 2, FromS);   // from(S)
       t4jn.api.getArg(FromS, 0, Seller); // S

       t4jn.api.getArg(Res0, 3, PriceP);  // price(P)
       t4jn.api.getArg(PriceP, 0, Price); // P

       .print("Received proposal from ", Seller, " for book ", BookTitle,
            " (target is ", TargetBookTitle, "), price is ", Price);
            
       +proposal(Seller);

       if (Price == "unavailable") {
            .print("Book is NOT available from ", Seller, " :/");
       } else {
            .print("Book is available from ", Seller, " :)");

            ?best_seller(BestSeller);
            ?best_price(BestPrice);
//            .print("old best seller = ", BestSeller, " old best price = ", BestPrice);

            if (BestSeller == null) { // update mental notes
                -best_seller(BestSeller);
                +best_seller(Seller);
                -best_price(BestPrice);
                +best_price(Price);
//                .print("non-null best seller = ", Seller, " non-null best price = ", Price);
            } else {
                if (Price < BestPrice) {
                    -best_seller(BestSeller);
                    +best_seller(Seller);
                    -best_price(BestPrice);
                    +best_price(Price);
//                    .print("new best seller = ", Seller, " new best price = ", Price);
                }
            }
       }
       
       .count(proposal(_), N);
       .count(seller(_), M);
       if (N == M) {
           ?best_seller(BestSeller2);
           if (BestSeller2 \== null) {
               .abolish(proposal(_));
               !purchase;
           } else {
               .print("Book is NOT available :/");
               -target_book_title(TargetBookTitle);
               .abolish(proposal(_));
               .abolish(seller(_));
               -+best_seller(null);
               -+best_price(null);
               !boot;
           }
       } else {
           !collect_proposals(TargetBookTitle);
       }.

+!purchase
    <- .my_name(Me);
       ?target_book_title(TargetBookTitle);
       ?best_seller(BestSeller);
       ?best_price(BestPrice);

       .print("Sending purchase order for book ", TargetBookTitle,
            " to agent ", BestSeller, "...");
       t4jn.api.out("default", "127.0.0.1", "20504",
            order(accept, from(Me), to(BestSeller), book(TargetBookTitle)), Out1);

       .findall(X, seller(X), Sellers); // find all sellers stored as mental notes
       for ( .member(Seller, Sellers) ) {
            if ( not (Seller == BestSeller) ) {
                .print("Sending reject for book ", TargetBookTitle, " to agent ", Seller);
                t4jn.api.out("default", "127.0.0.1", "20504",
                order(reject, from(Me), to(Seller), book(TargetBookTitle)), Out2);
            }
       }
       .abolish(seller(_));
       -+best_price(null);
       !wait_for_confirmation.

+!wait_for_confirmation
    <- .my_name(Me);
       ?best_seller(BestSeller);
       ?target_book_title(TargetBookTitle);
       
       .print("Waiting for purchase confirmation message...");
       t4jn.api.in("default", "127.0.0.1", "20504",
            purchase(C, to(Me), from(BestSeller), book(TargetBookTitle)), In1);
       t4jn.api.getResult(In1, Res1);

       t4jn.api.getArg(Res1, 0, Confirm); // C

       if (not (Res1 == null)) {
            .print("Received confirmation from ", BestSeller);
       }
       if (Confirm == "confirm") {
            .print("Book ", TargetBookTitle,
                " has been successfully purchased from agent ",
                BestSeller);
       } else {
            .print("Book ", TargetBookTitle, " has already been sold :/");
       }
       -target_book_title(TargetBookTitle);
       -+best_seller(null);
       !boot.
