/*
 * Copyright 1999-2015 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of TuCSoN4Jason <http://tucson4jason.apice.unibo.it>.
 *
 *    TuCSoN4Jason is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    TuCSoN4Jason is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with TuCSoN4Jason.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package it.unibo.tucson4jason.architecture;

import it.unibo.tucson4jason.operations.TucsonResult;
import jason.architecture.AgArch;
import jason.asSemantics.Intention;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;
import alice.tucson.asynchSupport.AsynchOpsHelper;
import alice.tucson.asynchSupport.actions.AbstractTucsonOrdinaryAction;

/**
 * Class that represents the custom architecture for a Jason agent to be used
 * with TuCSoN.
 *
 * @author Alessandro Fantini (mailto: alessandro.fantini3@studio.unibo.it)
 * @author Federico Foschini (mailto: federico.foschini4@studio.unibo.it)
 */
public class T4JnArchImpl extends AgArch implements T4JnArch {

    private static final String VERSION = "TuCSoN4Jason-1.0";
    private static AtomicBoolean first = new AtomicBoolean(true);

    private final boolean logEnabled = true;

    private final Logger logger = Logger.getLogger(this.getClass()
            .getSimpleName());

    /**
     * Variable to assign unique IDs to TuCSoN operations and get results.
     */
    private long counter;

    /**
     * Helper TuCSoN agent to delegate asynchronous operations to.
     */
    private AsynchOpsHelper helper;

    /**
     * Collection used to store results from TuCSoN operations.
     */
    private Map<Long, TucsonResult> results;

    /**
     * Collection used to store suspended intentions waiting for result.
     */
    private Map<Long, Intention> suspendedIntentions;

    /**
     * Mutex.
     * 
     * @see {@link T4JnArch}
     */
    private Lock mutex;

    @Override
    public final long addTucsonOperationRequest(
            final AbstractTucsonOrdinaryAction action) {
        this.helper.enqueue(action, new TucsonResultsHandler(this.counter,
                this.helper, this.results, this.suspendedIntentions,
                this.mutex, this.getTS(), this.logEnabled));
        return this.counter++;
    }

    @Override
    public final void init() throws Exception {
        if (first.compareAndSet(true, false)) {
            logHeader();
        }
        this.counter = 0L;
        this.helper = new AsynchOpsHelper("helper4" + this.getAgName());
        this.results = new HashMap<>();
        this.suspendedIntentions = new HashMap<>();
        this.mutex = new ReentrantLock(true);
    }

    private void logHeader() {
        this.log("--------------------------------------------------------------------------------");
        this.log("Welcome to the TuCSoN4Jason (t4jn) bridge :)");
        this.log("  t4jn version " + T4JnArchImpl.getVersion());
        this.log(new Date().toString());
        this.log("--------------------------------------------------------------------------------");
    }

    @Override
    public final Map<Long, TucsonResult> getResults() {
        return this.results;
    }

    @Override
    public final Map<Long, Intention> getSuspendedIntentions() {
        return this.suspendedIntentions;
    }

    @Override
    public final Lock getMutex() {
        return this.mutex;
    }

    private static String getVersion() {
        return VERSION;
    }

    private final void log(final String msg) {
        if (this.logEnabled) {
            this.logger.info(msg);
        }
    }

}
