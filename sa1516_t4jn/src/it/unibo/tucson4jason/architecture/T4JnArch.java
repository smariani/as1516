/*
 * Copyright 1999-2015 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of TuCSoN4Jason <http://tucson4jason.apice.unibo.it>.
 *
 *    TuCSoN4Jason is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    TuCSoN4Jason is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with TuCSoN4Jason.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package it.unibo.tucson4jason.architecture;

import it.unibo.tucson4jason.operations.TucsonResult;
import jason.asSemantics.Intention;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import alice.tucson.asynchSupport.actions.AbstractTucsonOrdinaryAction;

/**
 * Interface that represents the custom architecture for a Jason agent to be
 * used with TuCSoN.
 *
 * @author Alessandro Fantini (mailto: alessandro.fantini3@studio.unibo.it)
 * @author Federico Foschini (mailto: federico.foschini4@studio.unibo.it)
 */
public interface T4JnArch {

    /**
     * Method to add a TuCSoN operation to execute.
     *
     * @param action
     *            The TuCSoN operation.
     * @return The ID of the TuCSoN operation.
     */
    long addTucsonOperationRequest(AbstractTucsonOrdinaryAction action);

    /**
     * Method to get the collection of TuCSoN operation results.
     *
     * @return The collection of TuCSoN operation results.
     */
    Map<Long, TucsonResult> getResults();

    /**
     * Method to get the collection of suspended intentions waiting for result.
     *
     * @return The collection of suspended intentions waiting for result.
     */
    Map<Long, Intention> getSuspendedIntentions();

    /**
     * Method to get the mutex granting exclusive access to results and
     * suspended intentions collections.
     *
     * @return The mutex granting exclusive access to results and suspended
     *         intentions collections.
     */
    Lock getMutex();

}
