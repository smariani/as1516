/*
 * Copyright 1999-2015 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of TuCSoN4Jason <http://tucson4jason.apice.unibo.it>.
 *
 *    TuCSoN4Jason is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    TuCSoN4Jason is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with TuCSoN4Jason.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package it.unibo.tucson4jason.architecture;

import it.unibo.tucson4jason.operations.TucsonResult;
import jason.asSemantics.Circumstance;
import jason.asSemantics.Intention;
import jason.asSemantics.TransitionSystem;
import jason.asSyntax.ListTerm;
import jason.asSyntax.ListTermImpl;
import jason.asSyntax.Literal;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;
import jason.stdlib.suspend;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.logging.Logger;
import t4jn.api.getResult;
import alice.logictuple.LogicTuple;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.TucsonOperationCompletionListener;
import alice.tucson.asynchSupport.AsynchOpsHelper;
import alice.tuplecentre.api.Tuple;
import alice.tuplecentre.core.AbstractTupleCentreOperation;

/**
 * Listener used to handle TuCSoN operations completion.
 *
 * @author Alessandro Fantini (mailto: alessandro.fantini3@studio.unibo.it)
 * @author Federico Foschini (mailto: federico.foschini4@studio.unibo.it)
 */
class TucsonResultsHandler implements TucsonOperationCompletionListener {

    private boolean logEnabled = true;
    private final Logger logger = Logger.getLogger(this.getClass()
            .getSimpleName());

    /**
     * Unique ID for a TuCSoN operation.
     */
    private long actionId;

    /**
     * Helper TuCSoN agent to delegate asynchronous operations to.
     */
    private AsynchOpsHelper helper;

    /**
     * Collection used to store results from TuCSoN operations.
     */
    private Map<Long, TucsonResult> results;

    /**
     * Collection used to store suspended intentions waiting for result.
     */
    private Map<Long, Intention> suspendedIntentions;

    /**
     * Mutex.
     * 
     * @see {@link T4JnArch}
     */
    private Lock mutex;

    /**
     * The Transition System.
     */
    private TransitionSystem ts;

    /**
     * The only constructor.
     * 
     * @param actionId
     *            the ID of the Jason action.
     * @param helper
     *            the TuCSoN helper for asynchronous operations handling.
     * @param results
     *            the map of TuCSoN operations results.
     * @param suspendedIntentions
     *            the map of Jason suspended intentions.
     * @param mutex
     *            the mutex (see {@link T4JnArch})
     * @param ts
     *            the Jason transition system
     * @param logEnabled
     *            whether logging is enabled.
     */
    public TucsonResultsHandler(final long actionId,
            final AsynchOpsHelper helper,
            final Map<Long, TucsonResult> results,
            final Map<Long, Intention> suspendedIntentions, final Lock mutex,
            final TransitionSystem ts, final boolean logEnabled) {
        this.actionId = actionId;
        this.helper = helper;
        this.results = results;
        this.suspendedIntentions = suspendedIntentions;
        this.mutex = mutex;
        this.ts = ts;
        this.logEnabled = logEnabled;
    }

    @Override
    public final void operationCompleted(final AbstractTupleCentreOperation arg0) {

        TucsonResult res;
        if (arg0.getTupleListResult() == null) {
            // result is a LogicTuple
            res = new TucsonResultImpl(arg0.getTupleResult());
        } else {
            // result is a list of LogicTuple
            final List<Tuple> list = new ArrayList<Tuple>();
            list.addAll(arg0.getTupleListResult());
            res = new TucsonResultImpl(list);
        }

        this.log("Operation #" + this.actionId + " completed with result: "
                + res);

        /* remove completed TuCSoN operation */
        this.helper.getCompletedOps().removeOpById(arg0.getId());
        this.mutex.lock(); // acquire

        if (!(this.suspendedIntentions.containsKey(this.actionId))) {

            this.results.put(this.actionId, res);
            this.mutex.unlock(); // release

        } else {

            Intention suspendedIntention = this.suspendedIntentions
                    .remove(this.actionId);
            this.mutex.unlock(); // release
            final Term resultTerm;
            if (!res.isList()) { // result is a LogicTuple
                final LogicTuple logicTuple = (LogicTuple) res.getTuple();
                final Literal tuple = Literal.parseLiteral(logicTuple
                        .toString());
                resultTerm = tuple;
            } else { // result is a list of LogicTuple
                final ListTerm list = ListTermImpl.parseList(res.getTuples()
                        .toString());
                resultTerm = list;
            }

            Circumstance c = this.ts.getC();
            // Search the goal in PI
            Iterator<String> ik = c.getPendingIntentions().keySet().iterator();
            while (ik.hasNext()) {
                String k = ik.next();
                if (k.startsWith(suspend.SUSPENDED_INT)) {
                    Intention i = c.getPendingIntentions().get(k);
                    if (i.equals(suspendedIntention)) { // OUR MOD: equals
                        i.setSuspended(false);
                        ik.remove();
                        // remove the IA .suspend in case of self-suspend
                        if (k.startsWith(suspend.SELF_SUSPENDED_INT)) {
                            Structure st = (Structure) i.peek()
                                    .removeCurrentStep();
                            i.peek()
                                    .getUnif()
                                    .unifies(
                                            st.getTerm(getResult.RESULT_ARG_INDEX),
                                            resultTerm);
                        }
                        // add it back in I if not in PA
                        if (!c.getPendingActions().containsKey(i.getId()))
                            c.resumeIntention(i);

                    }
                }
            }

        }

    }

    @Override
    public void operationCompleted(final ITucsonOperation arg0) {
        // not used
    }

    /*
     * (non-Javadoc) Method to log a message.
     * @param msg The message to log.
     */
    protected final void log(final String msg) {
        if (this.logEnabled) {
            this.logger.info(msg);
        }
    }

}
