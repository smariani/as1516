/*
 * Copyright 1999-2015 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of TuCSoN4Jason <http://tucson4jason.apice.unibo.it>.
 *
 *    TuCSoN4Jason is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    TuCSoN4Jason is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with TuCSoN4Jason.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package it.unibo.tucson4jason.architecture;

import it.unibo.tucson4jason.operations.TucsonResult;
import java.util.List;
import java.util.stream.Collectors;
import alice.tuplecentre.api.Tuple;

/**
 * Class that represents the datatype of the results of TuCSoN operations.
 *
 * @author Alessandro Fantini (mailto: alessandro.fantini3@studio.unibo.it)
 * @author Federico Foschini (mailto: federico.foschini4@studio.unibo.it)
 */
class TucsonResultImpl implements TucsonResult {

    /**
     * The result of a TuCSoN operation that returns a list of Tuple.
     */
    private List<Tuple> tuples;

    /**
     * The result of a TuCSoN operation that returns a Tuple.
     */
    private Tuple tuple;

    /**
     * Constructor for the result of a TuCSoN operation that returns a list of
     * Tuple.
     *
     * @param result
     *            The TuCSoN operation result as a list of Tuple.
     */
    public TucsonResultImpl(final List<Tuple> result) {
        this.tuples = result;
        this.tuple = null;
    }

    /**
     * Constructor for the result of a TuCSoN operation that returns a Tuple.
     *
     * @param result
     *            The TuCSoN operation result as a Tuple.
     */
    public TucsonResultImpl(final Tuple result) {
        this.tuple = result;
        this.tuples = null;
    }

    @Override
    public final List<Tuple> getTuples() {
        return this.tuples;
    }

    @Override
    public final Tuple getTuple() {
        return this.tuple;
    }

    @Override
    public final boolean isList() {
        return this.tuples != null;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        if (this.isList()) {
            return this.tuples.stream().map(e -> e.toString())
                    .collect(Collectors.joining(", ", "[ ", " ]"));
        }
        return this.tuple.toString();
    }

}
