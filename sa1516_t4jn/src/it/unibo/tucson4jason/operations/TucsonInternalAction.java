/*
 * Copyright 1999-2015 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of TuCSoN4Jason <http://tucson4jason.apice.unibo.it>.
 *
 *    TuCSoN4Jason is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    TuCSoN4Jason is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with TuCSoN4Jason.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package it.unibo.tucson4jason.operations;

import jason.asSemantics.InternalAction;

/**
 * Interface that represents TuCSoN internal actions.
 *
 * @author Alessandro Fantini (mailto: alessandro.fantini3@studio.unibo.it)
 * @author Federico Foschini (mailto: federico.foschini4@studio.unibo.it)
 */
public interface TucsonInternalAction extends InternalAction {

    /**
     * The index of Tuple Centre Network ID for a TuCSoN internal action.
     */
    int NETID_ARG_INDEX = 1;

    /**
     * The index of Tuple Centre Port Number for a TuCSoN internal action.
     */
    int PORTNO_ARG_INDEX = 2;

    /**
     * The index of Tuple Centre Name for a TuCSoN internal action.
     */
    int TCNAME_ARG_INDEX = 0;

    /**
     * The minimum number of arguments for a TuCSoN internal action.
     */
    int TUCSON_INTERNAL_ACTION_MIN_ARGS = 3;

}
