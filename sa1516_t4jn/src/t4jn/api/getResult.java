/*
 * Copyright 1999-2015 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of TuCSoN4Jason <http://tucson4jason.apice.unibo.it>.
 *
 *    TuCSoN4Jason is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    TuCSoN4Jason is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with TuCSoN4Jason.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package t4jn.api;

import it.unibo.tucson4jason.architecture.T4JnArch;
import it.unibo.tucson4jason.operations.TucsonResult;
import jason.JasonException;
import jason.asSemantics.Circumstance;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.Intention;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ListTerm;
import jason.asSyntax.ListTermImpl;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.logging.Logger;
import alice.logictuple.LogicTuple;

/**
 * The getResult internal action. This internal action is used to retrieve the
 * result of a TuCSoN operation over a tuple centre.
 *
 * @author Alessandro Fantini (mailto: alessandro.fantini3@studio.unibo.it)
 * @author Federico Foschini (mailto: federico.foschini4@studio.unibo.it)
 */
public class getResult extends DefaultInternalAction {

    private static final long serialVersionUID = 1L;

    private final boolean logEnabled = true;

    private final Logger logger = Logger.getLogger(this.getClass()
            .getSimpleName());

    /**
     * True if intention must be suspended, false otherwise.
     */
    private boolean suspendIntention;

    /**
     * The index of the action ID.
     */
    public static final int ACTIONID_ARG_INDEX = 0;

    /**
     * The index of Result variable.
     */
    public static final int RESULT_ARG_INDEX = 1;

    /**
     * Internal action body.
     *
     * The argument configuration by index: 0 TuCSoN operation ID, 1 variable to
     * be unified with the result.
     */
    @Override
    public final Object execute(final TransitionSystem ts, final Unifier un,
            final Term[] args) throws Exception {

        this.suspendIntention = false; // by default, don't suspend the
                                       // intention

        /* get agent's custom architecture */
        T4JnArch arch;
        if (ts.getUserAgArch() instanceof T4JnArch) {
            arch = (T4JnArch) ts.getUserAgArch();
        } else {
            throw new JasonException("[" + this.getClass().getSimpleName()
                    + "]: architecture mismatch");
        }

        /* check arguments */
        checkArguments(args);

        /* args[ACTIONID_ARG_INDEX] */
        if (!args[getResult.ACTIONID_ARG_INDEX].isGround()) {
            return false;
        }
        final long actionId = Long.parseLong(args[ACTIONID_ARG_INDEX]
                .toString().replaceAll("\"", ""));

        /* args[RESULT_ARG_INDEX] */
        if (!args[RESULT_ARG_INDEX].isVar()) {
            return false;
        }

        final Map<Long, TucsonResult> results = arch.getResults();
        final Lock mutex = arch.getMutex();

        /* exclusive access to results and suspended intentions collections */
        mutex.lock();

        if (!(results.containsKey(actionId))) { // result is NOT available yet
            Circumstance c = ts.getC();
            /* suspend the current intention */
            Intention i = c.getSelectedIntention();
            this.suspendIntention = true;
            i.setSuspended(true);
            c.addPendingIntention(
                    jason.stdlib.suspend.SELF_SUSPENDED_INT + i.getId(), i);
            /* add current intention to map of suspended intentions */
            arch.getSuspendedIntentions().put(actionId, i);
            mutex.unlock(); // release
            return true;
        }

        TucsonResult res = results.remove(actionId);
        mutex.unlock(); // release

        if (!res.isList()) { // result is a LogicTuple
            final LogicTuple logicTuple = (LogicTuple) res.getTuple();
            if (logicTuple == null) {
                return true; // result not available, no need to unify
            }
            final Literal tuple = Literal.parseLiteral(logicTuple.toString());
            return un.unifies(args[RESULT_ARG_INDEX], tuple);
        }

        final ListTerm list = ListTermImpl
                .parseList(res.getTuples().toString());
        if (list == null) {
            return true; // result not available, no need to unify
        }
        return un.unifies(args[RESULT_ARG_INDEX], list);

    }

    /**
     * The maximum number of arguments is 2.
     *
     * Usage: getResult(actionId, Result);
     */
    @Override
    public final int getMaxArgs() {
        return 2;
    }

    /**
     * The maximum number of arguments is 2.
     *
     * Usage: getResult(actionId, Result);
     */
    @Override
    public final int getMinArgs() {
        return 2;
    }

    @Override
    public final boolean suspendIntention() {
        return this.suspendIntention;
    }

    /**
     * Method to log a message.
     * 
     * @param msg
     *            The message to log.
     */
    protected final void log(final String msg) {
        if (this.logEnabled) {
            this.logger.info(msg);
        }
    }

}
