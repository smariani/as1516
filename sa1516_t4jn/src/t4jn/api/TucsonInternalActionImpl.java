/*
 * Copyright 1999-2015 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of TuCSoN4Jason <http://tucson4jason.apice.unibo.it>.
 *
 *    TuCSoN4Jason is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    TuCSoN4Jason is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with TuCSoN4Jason.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package t4jn.api;

import it.unibo.tucson4jason.architecture.T4JnArch;
import it.unibo.tucson4jason.operations.TucsonInternalAction;
import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.StringTermImpl;
import jason.asSyntax.Term;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.asynchSupport.actions.AbstractTucsonOrdinaryAction;

/**
 * Class that represents TuCSoN internal actions.
 *
 * @author Alessandro Fantini (mailto: alessandro.fantini3@studio.unibo.it)
 * @author Federico Foschini (mailto: federico.foschini4@studio.unibo.it)
 */
abstract class TucsonInternalActionImpl extends DefaultInternalAction implements
        TucsonInternalAction {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The the custom architecture for a Jason agent to be used with TuCSoN.
     */
    private T4JnArch arch;

    /**
     * The Tuple Centre ID.
     */
    private TucsonTupleCentreId tid;

    @Override
    public final Object execute(final TransitionSystem ts, final Unifier un,
            final Term[] args) throws Exception {
        this.configure(ts, args);
        final AbstractTucsonOrdinaryAction action = this
                .generateTucsonOperation(args, this.tid);
        final long actionId = this.arch.addTucsonOperationRequest(action);
        return un.unifies(args[this.getResultArgumentIndex()],
                new StringTermImpl("" + actionId));
    }

    @Override
    public final int getMaxArgs() {
        return this.getNumberOfArguments();
    }

    @Override
    public final int getMinArgs() {
        return this.getNumberOfArguments();
    }

    /**
     * Method to configure everything that is needed to execute TuCSoN internal
     * actions.
     * 
     * @param ts
     *            The transition system.
     * @param args
     *            The TuCSoN internal action arguments.
     * @throws JasonException
     *             Jason exception.
     */
    protected final void configure(final TransitionSystem ts, final Term[] args)
            throws JasonException {
        if (ts.getUserAgArch() instanceof T4JnArch) {
            this.arch = (T4JnArch) ts.getUserAgArch();
        } else {
            throw new JasonException("[" + this.getClass().getSimpleName()
                    + "]: architecture mismatch");
        }

        this.checkArguments(args);

        try {
            this.tid = new TucsonTupleCentreId(
                    args[TucsonInternalAction.TCNAME_ARG_INDEX].toString()
                            .replaceAll("\"", ""),
                    args[TucsonInternalAction.NETID_ARG_INDEX].toString()
                            .replaceAll("\"", ""),
                    args[TucsonInternalAction.PORTNO_ARG_INDEX].toString()
                            .replaceAll("\"", ""));
        } catch (final TucsonInvalidTupleCentreIdException e) {
            e.printStackTrace();
            throw new JasonException("[" + this.getClass().getSimpleName()
                    + "]: invalid tuple centre id");
        }
    }

    /**
     * Method to generate the TuCSoN operation to execute.
     *
     * @param args
     *            The arguments of the internal action.
     * @param tcid
     *            The Tuple Centre ID.
     * @return The TuCSoN operation to execute.
     * @throws InvalidLogicTupleException
     *             Invalid Logic Tuple Exception.
     */
    protected abstract AbstractTucsonOrdinaryAction generateTucsonOperation(
            final Term[] args, final TucsonTupleCentreId tcid)
            throws InvalidLogicTupleException;

    /**
     * Method to get the number of arguments needed for a specific TuCSoN
     * internal action.
     *
     * @return The number of arguments needed for a specific TuCSoN internal
     *         action.
     */
    protected abstract int getNumberOfArguments();

    /**
     * Method to get the index of Result variable for a TuCSoN internal action.
     *
     * @return The index of Result variable for a TuCSoN internal action.
     */
    protected abstract int getResultArgumentIndex();

}
