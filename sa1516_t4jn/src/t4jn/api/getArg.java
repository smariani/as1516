/*
 * Copyright 1999-2015 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of TuCSoN4Jason <http://tucson4jason.apice.unibo.it>.
 *
 *    TuCSoN4Jason is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    TuCSoN4Jason is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with TuCSoN4Jason.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package t4jn.api;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Literal;
import jason.asSyntax.StringTermImpl;
import jason.asSyntax.Term;
import java.util.logging.Logger;

/**
 * The getArg internal action. This internal action is used to retrieve an
 * argument from a tuple, given the argument's index.
 *
 * @author Alessandro Fantini (mailto: alessandro.fantini3@studio.unibo.it)
 * @author Federico Foschini (mailto: federico.foschini4@studio.unibo.it)
 */
public class getArg extends DefaultInternalAction {

    private static final long serialVersionUID = 1L;

    private final boolean logEnabled = true;

    private final Logger logger = Logger.getLogger(this.getClass()
            .getSimpleName());

    /**
     * The index of the tuple.
     */
    protected static final int TUPLE_ARG_INDEX = 0;

    /**
     * The index of the argument's index.
     */
    protected static final int INDEX_ARG_INDEX = 1;

    /**
     * The index of Result variable.
     */
    protected static final int RESULT_ARG_INDEX = 2;

    @Override
    public final Object execute(final TransitionSystem ts, final Unifier un,
            final Term[] args) throws Exception {

        checkArguments(args);

        Literal tuple = Literal.parseLiteral(args[TUPLE_ARG_INDEX].toString()
                .replaceAll("\"", ""));

        /* args[INDEX_ARG_INDEX] */
        if (!args[INDEX_ARG_INDEX].isGround()) {
            return false;
        }
        int index = Integer.parseInt(args[INDEX_ARG_INDEX].toString());

        /* args[RESULT_ARG_INDEX] */
        if (!args[RESULT_ARG_INDEX].isVar()) {
            return false;
        }

        if (index < 0 || index > tuple.getArity() - 1) {
            throw new Exception("index of out bounds exception");
        }

        return un.unifies(args[RESULT_ARG_INDEX], new StringTermImpl(tuple
                .getTerm(index).toString()));
    }

    /**
     * The minimum number of arguments is 3.
     *
     * Usage: getArg(tuple, argIndex, Result);
     */
    @Override
    public final int getMinArgs() {
        return 3;
    }

    /**
     * The maximum number of arguments is 3.
     *
     * Usage: getArg(tuple, argIndex, Result);
     */
    @Override
    public final int getMaxArgs() {
        return 3;
    }

    /**
     * Method to log a message.
     * 
     * @param msg
     *            The message to log.
     */
    protected final void log(final String msg) {
        if (this.logEnabled) {
            this.logger.info(msg);
        }
    }

}
