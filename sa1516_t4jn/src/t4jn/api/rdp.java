/*
 * Copyright 1999-2015 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of TuCSoN4Jason <http://tucson4jason.apice.unibo.it>.
 *
 *    TuCSoN4Jason is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    TuCSoN4Jason is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with TuCSoN4Jason.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package t4jn.api;

import it.unibo.tucson4jason.operations.TucsonInternalAction;
import jason.asSyntax.Term;
import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.asynchSupport.actions.AbstractTucsonOrdinaryAction;
import alice.tucson.asynchSupport.actions.ordinary.Rdp;

/**
 * Class that represents the TuCSoN RDP operation.
 *
 * @author Alessandro Fantini (mailto: alessandro.fantini3@studio.unibo.it)
 * @author Federico Foschini (mailto: federico.foschini4@studio.unibo.it)
 */
public class rdp extends TucsonInternalActionImpl {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The index of Result variable for a TuCSoN internal action.
     */
    protected static final int RESULT_ARG_INDEX = 4;

    /**
     * The index of Tuple for a TuCSoN internal action.
     */
    protected static final int TUPLE_ARG_INDEX = 3;

    @Override
    protected final AbstractTucsonOrdinaryAction generateTucsonOperation(
            final Term[] args, final TucsonTupleCentreId tcid)
            throws InvalidLogicTupleException {
        final LogicTuple tuple = LogicTuple.parse(args[rdp.TUPLE_ARG_INDEX]
                .toString());
        return new Rdp(tcid, tuple);
    }

    /**
     * Method to get the number of arguments needed for a specific TuCSoN
     * internal action.
     *
     * The argument configuration by index: 0 tcName 1 netid 2 portno 3 tuple 4
     * result variable to be unified with the TuCSoN action ID.
     *
     * @return The number of arguments needed for a specific TuCSoN internal
     *         action.
     */
    @Override
    protected final int getNumberOfArguments() {
        return TucsonInternalAction.TUCSON_INTERNAL_ACTION_MIN_ARGS + 2;
    }

    @Override
    protected final int getResultArgumentIndex() {
        return rdp.RESULT_ARG_INDEX;
    }

}
